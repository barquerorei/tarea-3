/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea.pkg3.semana2;

import java.util.Scanner;

/**
 *
 * @author Chela
 */
public class Tarea3SEMANA2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    Scanner dato = new Scanner(System.in);

        int opc;

        System.out.println("-----Menu de opciones----");
        System.out.println("    1 Edad Actual        ");
        System.out.println("    2 Salario            ");
        System.out.println("    3 Numero hasta el 20 ");
        System.out.println("    4 Promedio de nota   "); 
        System.out.println("    5 Salir              "); 
        System.out.println("-------------------------"); 
        opc = dato.nextInt();

        switch (opc) {
            case 1:
                EdadActual();
                break;

            case 2:
                Salario();
                break;

            case 3:
                Numero();
                break;
            case 4:
               Promedio();
                break;
            case 5:
                break;
            
           
        } // Fin de switch opc

    }//Fin del main

    private static void EdadActual() {
        Scanner dato1 = new Scanner(System.in);
        Scanner dato2 = new Scanner(System.in);
        int añoa;
        int añon;
        int edad = 0;
        String SN = "";
        
        System.out.println("Digite el año actual:");
        añoa = dato1.nextInt(); //Pide el año actual
        System.out.println("Digite el año en el que nació:");
        añon = dato1.nextInt();//Pide el año de nacimiento 
        edad = (añoa - añon);//Edad del usuario

        System.out.println("¿Usted ya cumplio años?");//Pide al Usuario decir si ya cumplio años 
        System.out.println("----Ingrese si / no----");// Depende de esto se modifica la edad
        SN = dato2.nextLine();

        if (SN.equals("si")) {
            System.out.println("Su edad actual es" + " " + edad);//Queda idual
        } 
        else if (SN.equals("no"));
        {
            edad = edad - 1;
            System.out.println("Su edad actual es" + " " + edad);//se resta un año

        }

    } // Fin de Edad Actual

    private static void Salario() {
        Scanner cant1 = new Scanner(System.in);
        int horasL;
        int horaP;
        int salarioBRUTO;
        int salarioNETO;
        int deducciones;

        System.out.println("Ingrese la cantidad de horas laboradas en el mes:");
        horasL = cant1.nextInt();
        System.out.println("Digite el precio por hora:");
        horaP = cant1.nextInt();

        salarioBRUTO = (horasL * horaP);
        salarioNETO = (int) (salarioBRUTO - (salarioBRUTO * (9.17 / 100)));
        deducciones = salarioBRUTO - salarioNETO;

        System.out.println("El salario bruto es:" + salarioBRUTO);
        System.out.println("El salario neto es:" + salarioNETO);
        System.out.println("Las deducciones son:" + deducciones);
        
    }//Fin de Salario

    private static void Numero(){ 
        Scanner num = new Scanner(System.in);
        int i = 1;
        
        while (i <= 20){
        System.out.println(i);
        i = i+ 1;//Incrementa de uno en uno
        
        }//Fin de while   
    
    }// Fin de clase numero
    
    private static void Promedio(){ 
        Scanner num = new Scanner(System.in);
        int i = 0;
        double prom = 0;
        double nota = 0;
        
        while (i <= 2) {//lo hice con 3 y me pide 4 notas ***
        System.out.println("Ingrese la nota a calcula: ");
        nota = num.nextInt();
        prom = prom + nota;
        i = i + 1;
        }//Fin while
      prom = prom / 3;
       System.out.println("Su promedio es: "+" "+prom); 
        
        
        
    }// Fin de clase Promedio
}

